package com.lihaishan.filesave;

import com.lihaishan.filesave.config.Sysconfig;
import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
//@RestController
//@EnableConfigurationProperties(Sysconfig.class)
public class FilesaveApplication {
//    @RequestMapping("/")
//    String index(){
//        return "Spring hello world";
//    }

    public static void main(String[] args) {
        SpringApplication application = new SpringApplication(FilesaveApplication.class);
//        SpringApplication.run(FilesaveApplication.class, args);
        /**
         * 控制启动图案，可以再resource下新建banner.txt
         *         OFF,
         *         CONSOLE,
         *         LOG;
         */
        application.setBannerMode(Banner.Mode.CONSOLE);

        application.run(args);
    }


}
