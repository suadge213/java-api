package com.lihaishan.filesave;

import org.junit.jupiter.api.Test;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@SpringBootTest
class FilesaveApplicationTests {

    @Test
    void contextLoads() {
    }


    public static void main(String[] args) {
        SpringApplication.run(FilesaveApplication.class,args);
    }
}
