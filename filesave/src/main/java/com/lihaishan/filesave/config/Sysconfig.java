package com.lihaishan.filesave.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @Component
 *    @Value("${lihaishan.blog.name}")
 *    private String name;
 *    @Value("${lihaishan.blog.title}")
 *    private String tittle;
 */
@Data
//@ConfigurationProperties(prefix = "lihaishan.blog")
@Configuration
public class Sysconfig {
    @Value("${name}")
    private String name;
    @Value("${age}")
    private int age;

}
