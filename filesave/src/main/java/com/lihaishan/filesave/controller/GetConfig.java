package com.lihaishan.filesave.controller;

import com.lihaishan.filesave.config.Sysconfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GetConfig {
    @Autowired
    private Sysconfig sysconfig;
    @RequestMapping("/getconfig")
    String getProperties(){
        return sysconfig.getName()+"+++++"+sysconfig.getAge();
    }

}
